
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <h1>Hello, world!</h1>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>






<div class="cont">
  <div class="macdisplay">
    <div class="apple"></div>
    <div class="container">
      <div class="bubble">Xiteb</div>
    </div>
  </div>
  <div class="camera"></div>
  <div class="foot"></div>
</div>
<style>html, body {
  background: #222222;
  width: 600px;
  height: 100%;
  margin: 0 auto;
}

.container {
  background: linear-gradient(45deg, #1d0836, #cc50a3);
  width: 670px;
  height: 390px;
  margin: -60px 0 0 -100px;
  box-shadow: 0 0 5px black;
  transform: scale(0.7);
  z-index: 15;
}

.bubble {
  background: rgba(255, 255, 255, 0.1);
  width: 150px;
  height: 150px;
  border-radius: 50%;
  position: absolute;
  animation: play 7s infinite ease-in;
  box-shadow: 0 0 25px purple inset;
}
.bubble:before {
  background: rgba(255, 255, 255, 0.2);
  content: "";
  width: 60px;
  height: 28px;
  position: absolute;
  margin: 10px 0 0 25px;
  border-radius: 50%;
  transform: rotate(-20deg);
}

@keyframes play {
  0% {
    box-shadow: 0 0 25px purple inset;
    transform: red translateX(0) translateY(0px);
  }
  20% {
    box-shadow: 0 0 25px red inset;
    transform: translateX(300px) translateY(243px);
  }
  30% {
    box-shadow: 0 0 25px pink inset;
    transform: translateX(524px) translateY(140px);
  }
  40% {
    box-shadow: 0 0 25px violet inset;
    transform: translateX(300px) translateY(0);
  }
  50% {
    box-shadow: 0 0 25px purple inset;
    transform: translateX(100px) translateY(243px);
  }
  60% {
    box-shadow: 0 0 25px red inset;
    transform: translateX(0) translateY(180px);
  }
  70% {
    box-shadow: 0 0 25px pink inset;
    transform: translateX(200px) translateY(0);
  }
  80% {
    box-shadow: 0 0 25px violet inset;
    transform: translateX(524px) translateY(170px);
  }
  90% {
    box-shadow: 0 0 25px red inset;
    transform: translateX(300px) translateY(243px);
  }
}
.cont {
  width: 600px;
  margin: -120px 0 0 0;
  -webkit-transform: scale(0.6);
}

.macdisplay {
  background: #444444;
  width: 470px;
  height: 270px;
  border: solid 20px black;
  border-radius: 10px 10px 0 0;
  margin: 94px 0 0 40px;
  z-index: 1;
}
.macdisplay:before {
  background: -webkit-linear-gradient(top left, rgba(255, 255, 255, 0) 60%, rgba(255, 255, 255, 0.03) 60%, rgba(255, 255, 255, 0) 100%);
  content: "";
  width: 500px;
  height: 310px;
  border-radius: 10px 10px 0 0;
  margin: -20px 0 0 -10px;
  position: absolute;
  z-index: 16;
}
.macdisplay:after {
  background: #dddddd;
  background: linear-gradient(to right, #dddddd 0%, #cccccc 24%, #eeeeee 46%, #dddddd 69%, #cccccc 100%);
  content: "";
  width: 510px;
  height: 40px;
  position: absolute;
  margin: -41px 0 0 -20px;
  border-radius: 0 0 10px 10px;
  z-index: 3;
  box-shadow: 0 1px 2px #444444;
}

.camera {
  background: #222222;
  width: 2px;
  height: 2px;
  position: absolute;
  margin: -300px 0 0 293px;
  border-radius: 3px;
  box-shadow: 0 0 1px white;
}

.foot {
  background: #bcbcbc;
  background: linear-gradient(to bottom, #bcbcbc 0%, #eaeaea 22%, #dddddd 40%, #959595 50%, #a0a0a0 62%, #cccccc 70%, #cccccc 100%);
  width: 170px;
  height: 60px;
  position: absolute;
  margin: 40px 0 0 220px;
  border-radius: 0 0 4px 4px;
}
.foot:before {
  background: #222222;
  content: "";
  width: 60px;
  height: 62px;
  position: absolute;
  margin: 0 0 0 -35px;
  border-radius: 0 0 50px 0;
}
.foot:after {
  background: #222222;
  content: "";
  width: 60px;
  height: 62px;
  position: absolute;
  margin: 0 0 0 145px;
  border-radius: 0 0 0 50px;
}

.apple {
  background: black;
  width: 208px;
  height: 198px;
  position: absolute;
  margin: 210px 0 0 145px;
  border-radius: 36% 36% 41% 41%/42% 42% 75% 75%;
  transform: scale(0.1);
  z-index: 28;
}
.apple:before {
  background: black;
  content: "";
  width: 55px;
  height: 55px;
  position: absolute;
  margin: -60px 0 0 90px;
  border-radius: 100px 0;
  z-index: 29;
}
.apple:after {
  background: #eeeeee;
  content: "";
  width: 90px;
  height: 90px;
  position: absolute;
  margin: 28px 0 0 174px;
  border-radius: 50%;
  transform: rotate(-53deg);
  z-index: 4;
  box-shadow: 12px -158px 0 #eeeeee, -198px 3px 0 #eeeeee;
}

</style>

