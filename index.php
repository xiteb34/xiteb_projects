<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top navbar-collapsible">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<div class="container-fluid" style="padding: 0px ! important;">
   
    <section class="container-fluid" id="section1">
            <h2 class="text-center">Change this Content. Change the world.</h2>
            <div class="row">
              <div class="col-sm-12 ">
                <div id="boxWrapper">
                    <div class="box animated introAnimation">O</div>
                    <div class="box animated introAnimation two">u</div>
                    <div class="box animated introAnimation three">R</div>
                    <div class="box animated introAnimation four" style="visibility:hidden;"></div>
                    <div class="box animated introAnimation four">P</div>
                    <div class="box animated introAnimation five">R</div>
                    <div class="box animated introAnimation six">0</div>
                    <div class="box animated introAnimation seven">J</div>
                    <div class="box animated introAnimation eight">E</div>
                    <div class="box animated introAnimation nine">C</div>
                    <div class="box animated introAnimation ten">T</div>
                    <div class="box animated introAnimation eleven">S</div>
                   
                    
                </div>
               </div>
            </div>
    </section>
    
<!--/*    Designed for everyone, everywhere*/-->


     <section class="container-fluid" id="section2">
        <img style="width:150px" class="featurette-image img-responsive center-block operlogo" src="images/5for5.png">
            <h2 class="text-center 5for5 h2">Big Ideas in  small devices</h2>
            <h3 class="text-center  5for5 h3">( Entertainment Wap Portal )</h3>
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2">
              <img src="./images/bg_smartphones.jpg" class="img-responsive center-block ">
              <p class="text-center"><button type="button" class="btn btn-primary">View Demo</button>.</p>
              </div>
            </div>
    </section>
    
    
     <section class="container-fluid" id="section3">
       			 <h2 class="text-center">Best Projects</h2>
     
                <div class="row featurette">
                 <div class="col-md-3">
                    <div class="card"  >
                       <div class="face front"> <img  class="featurette-image img-responsive center-block s3img" src="images/iPadAir_Silver_en.png" data-holder-rendered="true">
                       </div>
                       <div class="face back"> <img  class="featurette-image img-responsive center-block s3img" src="images/iPadAir_Silver_en.png" data-holder-rendered="true"></div>
                     </div>
                     
                    </div>
                    
                    
                    <div class="col-md-3">
                    <div class="card">
                       <div class="face front"> <img  class="featurette-image img-responsive center-block s3img" src="images/iPadAir_Silver_en.png" data-holder-rendered="true"></div>
                       <div class="face back"> <img  class="featurette-image img-responsive center-block s3img" src="images/iPadAir_Silver_en.png" data-holder-rendered="true"></div>
                     </div>
                      
                    </div>
                    
                    
                    <div class="col-md-3">
                    <div class="card">
                       <div class="face front"> <img  class="featurette-image img-responsive center-block s3img" src="images/iPadAir_Silver_en.png" data-holder-rendered="true"></div>
                       <div class="face back"> <img  class="featurette-image img-responsive center-block s3img" src="images/iPadAir_Silver_en.png" data-holder-rendered="true"></div>
                     </div>

                      
                    </div>
                    
                     <div class="col-md-3">
                    <div class="card">
                       <div class="face front"> <img  class="featurette-image img-responsive center-block s3img" src="images/iPadAir_Silver_en.png" data-holder-rendered="true"></div>
                       <div class="face back"> <img  class="featurette-image img-responsive center-block s3img" src="images/iPadAir_Silver_en.png" data-holder-rendered="true"></div>
                     </div>

                      
                    </div>
                    
                    
                    
                  </div>
      
      </section>
      
     <section class="container-fluid" id="section4">
                <div class="row featurette">
                    <div class="col-md-4">
                     <div class="cloud-bar">
                       <div class="col-md-12">
                         <img style="width: 118px; margin-bottom: 25px" class="operlogo" src="images/amazon.png">
                       </div>
                       <div class="col-md-12"> 
                        <img src="images/Clouds.png" class="operlogo"  style="width: 100px;">
                       </div>
                     </div>
                    </div>
                    
                    
                    
                    <div class="col-md-4">
                     <div class="mac-ser" id="black-area">
                     <img  class="featurette-image img-responsive center-block" src="images/imac-large.png" data-holder-rendered="true">
                     <div class="absolut" id="screen"> <img  class="featurette-image img-responsive center-block evokelogo "  src="images/evoke.png" data-holder-rendered="true">
                      </div>
                     </div>
                    
                    </div>
                    
                    
                    <div class="col-md-4">
                      <h2 class="featurette-heading">Evoke International</h2>
                      <p class="sitefont">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>    <img src="images/all-operators.png" class="operlogo"  style=""> 
                    
                    </div>
                   
                  </div>
      
      </section>
      
 
      
      
      



    </div><!-- /.container -->

    
  </body>
   
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="js/jquery.min.js"></script>
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
     <script src="js/main.js"></script>
   
     
</html>








